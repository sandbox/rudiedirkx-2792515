
Install
--------------------

* Enable module
* On admin/config/content/ckeditor/edit/Advanced:
  * Enable CKEDITOR plugin "Webform Answers"
  * Add button to the editor
* On admin/config/content/formats/filtered_html:
  * Enable filter "Webform Answers"

Customize
--------------------

* On admin/structure/views/view/webform_answers_webforms/edit:
  * Edit the View until you like it
