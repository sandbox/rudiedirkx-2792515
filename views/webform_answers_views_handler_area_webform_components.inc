<?php

class webform_answers_views_handler_area_webform_components extends views_handler_area {

  /**
   * Set configurables.
   */
  function init(&$view, &$options) {
    parent::init($view, $options);

    $this->unselectable = array(
      'markup',
      'fieldset',
      'pagebreak',
    );
  }

  /**
   * Render the area.
   */
  function render($empty = FALSE) {
    if (!isset($this->view->args[0])) {
      return '';
    }

    $nid = $this->view->args[0];
    $node = node_load($nid);
    if (!$node) {
      return '';
    }

    $setting = array(
      'webformAnswers' => array(
        'webform' => array(
          'nid' => (int) $node->nid,
          'title' => $node->title,
          'components' => ezmod_always_array_column($node->webform['components'], 'form_key', 'cid'),
        ),
      ),
    );
    drupal_add_js($setting, array('type' => 'setting'));

    $components = $node->webform['components'];
    $get_depth = function($component) use ($components) {
      $depth = 0;
      while ($component['pid']) {
        $component = $components[ $component['pid'] ];
        $depth++;
      }
      return $depth;
    };

    $rows = array();
    foreach ($components as $cid => $component) {
      $depth = $get_depth($component);
      $id = 'webform-component-' . $nid . '-' . $cid;
      $selectable = !in_array($component['type'], $this->unselectable);

      $rows[] = array(
        'class' => array(
          'component-' . $component['type'],
          $selectable ? 'selectable' : 'not-selectable',
        ),
        'data' => array(
          $selectable ? '<input class="webform-component" type="checkbox" value="' . $cid . '" id="' . $id . '" />' : '',
          '<label for="' . $id . '">' . str_repeat('&nbsp; &nbsp; &nbsp;', $depth) . check_plain($component['name']) . '</label>',
        ),
      );
    }

    $vars = array(
      'header' => array('', t('Component')),
      'rows' => $rows,
      'attributes' => array('class' => array('webform-answers-component-selection')),
      'sticky' => FALSE,
    );
    $table = theme('table', $vars);

    $title = '<h2>' . check_plain($node->title) . '</h2>';

    $actions = '<p><input type="button" class="form-submit" value="' . t('Insert components') . '" id="form-submit-insert-components" /></p>';

    return $title . "\n" . $table . "\n" . $actions;
  }

}
