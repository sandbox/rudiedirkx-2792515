<?php

/**
 * Form callback for admin/config/content/webform-answers.
 */
function webform_answers_config_form($form, &$form_state) {
  $form['webform_answers_plugin_label'] = array(
    '#type' => 'textfield',
    '#title' => t('WYSIWYG plugin button label'),
    '#default_value' => variable_get('webform_answers_plugin_label', WEBFORM_ANSWERS_DEFAULT_PLUGIN_LABEL),
    '#description' => t('Will be translated.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
