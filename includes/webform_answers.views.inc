<?php

/**
 * Implements hook_views_data().
 */
function webform_answers_views_data() {

  // Footer area: custom components display.
  $data['views']['webform_components'] = array(
    'title' => t('Webform components'),
    'help' => t("Show the current Webform's components."),
    'area' => array(
      'handler' => 'webform_answers_views_handler_area_webform_components',
    ),
  );

  return $data;
}
