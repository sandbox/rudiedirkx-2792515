/**
 * @file
 * Plugin for inserting Webform Answers.
 */

(function($) {

  Drupal.webformAnswers || (Drupal.webformAnswers = {});

  // The handler that opens the Views grid in a modal.
  Drupal.webformAnswers.openModal = function(path) {
    // Prep options.
    var
      height = 0.8 * document.documentElement.clientHeight,
      width = 0.8 * document.documentElement.clientWidth,
      cancelText = Drupal.t('Close'),
      buttons = {};
    buttons[cancelText] = function(e) {
      var $modal = Drupal.webformAnswers.$modal;
      $modal.dialog('close');
    };

    // Create and extend options.
    var options = {
      "dialogClass": 'webform-answers-modal',
      "width": width,
      "height": height,
      "draggable": false,
      "modal": true,
      "overlay": {
        "backgroundColor": '#000000',
        "opacity": 0.4,
      },
      "resizable": false,
      "buttons": buttons,
      "titlebar": false,
    };

    // Create modal.
    var $modal = $('<div><iframe width="99%" height="99%" src="' + path + '"></iframe></div>');
    $('body').append($modal);
    $modal.dialog(options);

    // Remove title bar and reset height.
    if (!options.titlebar) {
      $modal.closest(".ui-dialog").find(".ui-dialog-titlebar").remove();
      $modal.dialog('option', 'height', height);
    }

    return Drupal.webformAnswers.$modal = $modal;
  };

  CKEDITOR.plugins.add('webform_answers', {
    "requires": ['fakeobjects'],
    "init": function(editor) {

      // Only load this plugin on pages that have a webformAnswers.plugin JS setting.
      if (!Drupal.settings.webformAnswers || !Drupal.settings.webformAnswers.plugin) {
        return;
      }

      var
        $textarea = $('#' + editor.name),
        field = $textarea.data('vsf-field');

      // Add Button.
      editor.ui.addButton('webform_answers', {
        "label": Drupal.settings.webformAnswers.plugin.label,
        "command": 'webform_answers',
        "icon": this.path + 'webform_answers.png'
      });

      // Add Command.
      editor.addCommand('webform_answers', {
        exec: function(editor) {
          var path = Drupal.settings.basePath + 'admin/webform-answers';
          if (Drupal.settings.webformAnswers && Drupal.settings.webformAnswers.webform) {
            path += '/' + Drupal.settings.webformAnswers.webform.nid;
          }

          Drupal.webformAnswers.editor = editor;
          Drupal.webformAnswers.openModal(path);
        }
      });

    }
  });

})(jQuery);
