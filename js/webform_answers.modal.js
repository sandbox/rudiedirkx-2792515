
(function($) {

  var owner = opener || parent;
  if (!owner || !owner.Drupal.webformAnswers || !owner.Drupal.webformAnswers.editor) return;

  var editor = owner.Drupal.webformAnswers.editor;

  Drupal.behaviors.webformAnswersModal = {
    attach: function(context, settings) {

      // Not all pages have this.
      if (!settings.webformAnswers || !settings.webformAnswers.webform) return;

      // Collect
      var webform = settings.webformAnswers.webform;

      $('#form-submit-insert-components', context).click(function(e) {
        e.preventDefault();

        var cids = $('.webform-answers-component-selection .webform-component:checked', context).map(function() {
          return parseInt(this.value);
        }).get();
        if (!cids.length) {
          alert(Drupal.t('Select at least one component.'));
          return;
        }

        for (var i=0; i<cids.length; i++) {
          var cid = cids[i];
          var form_key = webform.components[cid];
          if (form_key) {
            var text = Drupal.t('@title -- Answer: @answer_token -- Answered on: @created_token', {
              "@title": webform.title,
              "@answer_token": '[wa:' + webform.nid + ':' + cid + ':' + form_key + ':answer]',
              "@created_token": '[wa:' + webform.nid + ':created]'
            });
            editor.insertText(text);
          }
        }

        owner.Drupal.webformAnswers.$modal.dialog('close');
      });

    }
  };

})(jQuery);
